
public class LambdaTest{

  public static void main(String[] args){
   //Expresión lambda ==> representa un objeto de una interaz funcional
   FunctionTest ft=() -> System.out.println("Hola mundo");
   FunctionTest f=() -> System.out.println("Programa 1");


   ft.saludar();

   f.saludar();
}

}